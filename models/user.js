
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type : String,
		required : [true, "Please enter your first name."]
	},
	lastName: {
		type : String,
		required : [true, "Please enter your last name."]
	},
	email: {
		type : String,
		required : [true, "Please enter your email."]
	},
	password: {
		type : String,
		required : [true, "Please enter your password."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},


	cart: 
		[
			{
				productId: {
					type: String,
					required: [true, "Product Id required."]
				},
				quantity: {
					type: Number,
					required: [true, "Quantity required"]
				},
				totalPrice: {
					type: Number,
				}
			}

		]

});

module.exports = mongoose.model ("User", userSchema);