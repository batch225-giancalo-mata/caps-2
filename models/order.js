const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  	
  	userId: {
    	type: String,
    	required: true
  	},
  	
	products: [{
	    productId: {
	    	type: String,
	    	required: true
	   	},
	   	quantity: {
	       	type: Number,
	       	required: true
	   	},
	   	totalPrice: {
	   		type: Number,
	       	required: true
	   	}
	}],
  
  	totalAmount: {
	    type: Number,
	    required: true
  	},
  	
  	orderedOn: {
	    type: Date,
	    default: Date.now
  	}

});


module.exports = mongoose.model ("Order", orderSchema);