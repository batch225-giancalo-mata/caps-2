const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	
	name : {
		type : String,
		required : [true, "Please enter product name."]
	},
	description : {
		type : String,
		required : [true, "Please enter description and color."]
	},
	size : {
		type : String,
		required : [true, "Please enter product size."]
	},
	price : {
		type : Number,
		required : [true, "Please enter price."]
	},
	quantity : {
		type : Number,
		required : [true, "Please enter quantity."]
	},
	inStock : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},


	// orders : [
	// 	{
	// 		userId : {
	// 			type : String,
	// 			required: [true, "UserId is required"]
	// 		},
	// 		productDescription : {
	// 			type : String,
	// 			required: [true, "UserId is required"]
	// 		},
	// 		productSize : {
	// 			type : String,
	// 			required: [true, "UserId is required"]
	// 		},
	// 		orderedOn : {
	// 			type : Date,
	// 			default : new Date() 
	// 		}
	// 	}
	// ]
});


module.exports = mongoose.model ("Product", productSchema);