const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require ("dotenv").config();



// ================= ROUTES ==================



const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");



// ================= EXPRESS ==================



const app = express();
const port = 8000;

// CORS (Cross-Origin Resource Sharing) 

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));



// ================== MONGO DB ===================



mongoose.connect(`mongodb+srv://giancarlomata888:${process.env.PASSWORD}@cluster0.nezwiao.mongodb.net/product-ordering-api?retryWrites=true&w=majority`,

{
	useNewUrlParser : true,
	useUnifiedTopology : true
});


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB."));



// =================== ROUTING =====================



app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);



// ==================== SERVER PORT =====================



app.listen(port, () => console.log(`API is now online in port: ${port}`));
