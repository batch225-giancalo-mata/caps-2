// ================== EXPORT FUNCTIONS =====================



const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController")



// ================= USER REGISTRATION ROUTE ======================



router.post("/signUp", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});



// =================== USER AUTHENTICATION ROUTE =======================



router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});



// =================== RETRIEVE ALL USERS ROUTE ====================



router.get("/getAllUsers", auth.verify, (req, res) => {

	const { isAdmin } = auth.decode(req.headers.authorization);

	userController.getAllUsers(isAdmin).then(
		resultFromController => res.send(resultFromController));
});



// =================== RETRIEVE USER DETAILS ROUTE ====================



router.get("/getUser/:userId", (req, res) => {
    
    userController.getUser(req.params.userId).then(resultFromController => res.send(resultFromController));
});



// ================== SET AS ADMIN ROUTE =====================



router.put("/setAdmin/:userId", auth.verify, (req, res) => {
    
    const { isAdmin } = auth.decode(req.headers.authorization);

    userController.setAdmin(req.params.userId, isAdmin).then(resultFromController => {
        res.send(resultFromController);

    })
});



router.put("/removeAdmin/:userId", auth.verify, (req, res) => {
    
    const { isAdmin } = auth.decode(req.headers.authorization);

    userController.removeAdmin(req.params.userId, isAdmin).then(resultFromController => {
        res.send(resultFromController);

    })
});




module.exports = router;