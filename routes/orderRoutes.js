// ================== EXPORT FUNCTIONS =====================



const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController");

const productController = require("../controllers/productController");



// ================== ADD TO CART ROUTE =====================



router.post("/addToCart", auth.verify, (req, res) => {

	product = productController.getProduct(req.body.productId)

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId,
		quantity: req.body.quantity
	}

	orderController.addToCart(data).then(resultFromController => res.send(resultFromController));

})



// ================== CHECKOUT ROUTE =====================



router.post("/checkOut", auth.verify, (req, res) => {

	product = productController.getProduct(req.body.productId)

	let data = {
		userId : auth.decode(req.headers.authorization).id
	}

	orderController.checkOut(data).then(resultFromController => res.send(resultFromController));

})



// ================== RETRIEVE ORDERS ROUTE =====================

// // IN PROGRESS

// router.get("/getOrders/", auth.verify, (req, res) => {
	
// 	order = orderController.getOrders(req.body.productId)

// 	let data = {
// 		userId : auth.decode(req.headers.authorization).id
// 	}

// 	orderController.getOrders(userId, orderId).then(resultFromController => res.send(resultFromController));

// });





module.exports = router;
