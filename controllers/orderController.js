// ================ IMPORT/CALL FUNCTIONS ===================



const Product = require("../models/product");
const User = require("../models/user");
const Order = require("../models/order");
const productController = require("../controllers/productController");



// ======================= ADD TO CART FUNCTION =============================



module.exports.addToCart = async (data) => {
	
	let productPrice;
	let productName;
	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.quantity -= data.quantity
		productPrice = product.price
		productName = product.name
	
		return product.save().then((product, error) => {
	
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

	let isUserUpdated = await User.findById(data.userId).then(user => {
	user.cart.push({

		productId : data.productId,
		quantity: data.quantity,
		totalPrice: productPrice * data.quantity
	});

		return user.save().then((user, error) => {

			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});


	if(isUserUpdated && isProductUpdated){
	
		return {
			message: `Success! ${productName} has been added to cart.` 
		}

	} else {
		return false;
	};

};



// ===================== CHECKOUT =========================



module.exports.checkOut = async (data) => {

		let products = await User.findById(data.userId).then(user => user.cart)

		let totalAmount = 0
		products.forEach(product => {
			totalAmount += product.totalPrice
		});


		let new_order = new Order ({
			userId: data.userId,
			products,
			totalAmount
		})

		let orderAdded = await new_order.save().then((new_order, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})


		let isUserUpdated = await User.findById(data.userId).then(user => {
		
		user.cart = []

			return user.save().then((user, error) => {
				if (error) {
					return false;
				} else {
					return true;
				};
			});
		});


		if(orderAdded && isUserUpdated){
			return {
				message: `Success! Your order is in process. The products will be shipped to your address via drone within 24 hours.` 
			}

		} else {
			return false;
		};
	};



// ================ RETRIEVE ORDERS ==================

// IN PROGRESS

// module.exports.getOrders = (userId, orderId) => {
	
// 	let order = User.find({userId, _id: orderId});
// 	return order;

// };