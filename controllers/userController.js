// ================ IMPORT/CALL FUNCTIONS ===================



const User = require("../models/user");
const Product = require("../models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");



// ================= USER REGISTRATION - SIGN UP =================

// Checks if duplicate user/email already exists before sign up.


module.exports.registerUser = (reqBody) => {
  
  	return User.find({email : reqBody.email}).then(result => {
      
      	if (result.length > 0) {
        
        	return {
          		message: "User already exists. Please login."
        	}

    	} else {
        
        	let newUser = new User({
		        firstName : reqBody.firstName,
		        lastName : reqBody.lastName,
		        email : reqBody.email,
		        password : bcrypt.hashSync(reqBody.password, 10)
        
        	});

        return newUser.save().then(user => {
          	return user;
        
        })
    }

}).catch(err => {
       
        console.log(err);
	    throw new Error("An error occured. Please try again.");

    });
};



// ================== USER REGISTRATION - LOGIN =======================



module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return {
				message: "User not found. Please sign up to continue."
			}
		
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {

				return {
					message: `Welcome back ${result.firstName}!`,
					access : auth.createAccessToken(result)}

			} else {

				return {
					message: "Password was incorrect. Please try again."
				}
			};

		};
		
	}).catch(err => {
       
        console.log(err);
	    throw new Error("An error occured. Please try again.");

    });
};



// ================ RETRIEVE ALL USERS ===================



module.exports.getAllUsers = (isAdmin) => {
	
	if(isAdmin){
		
		return User.find({}).then(result => {
			return result;
		});
	
	} else {
	
		return Promise.resolve('Access denied. Only admins can perform this action.').catch(err => {
       
        console.log(err);
	    throw new Error("An error occured. Please try again.");

    });

	}
};



// ================ RETRIEVE USER DETAILS ===================



module.exports.getUser = (userId) => {
    
    return User.findById(userId).then(result => {
        return result;

    }).catch(err => {
       
        console.log(err);
	    throw new Error("An error occured. Please try again.");

    });;

};



// ============= GRANT ADMIN ACCESS (STRETCH GOAL) ==============



module.exports.setAdmin = (userId, isAdmin) => {
    
    if(isAdmin){
	    
	    let updateActiveField = {
	        isAdmin: true
	    };

	    return User.findByIdAndUpdate(userId, updateActiveField, {new: true }).then((updatedUser) => {
	        
		if (updatedUser == null) {
            return {
                message: "User not found."
            } 
        }

            return {
                message: "User has been granted admin access.",
                user: updatedUser
        }
    });
    
    } else {
    	
    	return Promise.resolve('Access denied. Only admins can perform this action.').catch(err => {
       
        console.log(err);
	    throw new Error("An error occured. Please try again.");

    });
    
    }
};



// ============= REMOVE ADMIN (STRETCH GOAL) ==============



module.exports.removeAdmin = (userId, isAdmin) => {
	
	if(isAdmin){ 
	
	    let updateActiveField = {
	        isAdmin: false
	    };

	    return User.findByIdAndUpdate(userId, updateActiveField, {new: true }).then((updatedUser) => {
	        
	       if (updatedUser == null) {
            return {
                message: "User not found."
            } 
        }

            return {
                message: "User has been removed from admin.",
                user: updatedUser
        }
    });
    
    } else {
    	
    	return Promise.resolve('Access denied. Only admins can perform this action.').catch(err => {
       
        console.log(err);
	    throw new Error("An error occured. Please try again.");

    });
    
    }
};


